import React from 'react';
import classnames from 'classnames';
import { Icon } from './icon';

export function MSButton(props) {
  let {
    children,
    type = 'secondary',
    size,
    large,
    outline = false,
    className,
    icon,
    ...rest
  } = props;

  let cl = {
    btn: true,
    'btn-lg': large,
    [(outline ? 'btn-outline-' : 'btn-') + type]: true,
    ['btn-' + size]: !!size,
    [className]: className,
  };

  return (
    <button type="submit" className={classnames(cl)} {...rest}>{icon && <Icon icon={icon} />} {children}</button>
  );
}

export function MSButtonGroup(props) {
  let {
    children,
    ...rest
  } = props;
  return <div className="btn-group" {...rest}>{children}</div>;
}