/* eslint-disable jsx-a11y/label-has-for */
import React from 'react';
import classnames from 'classnames';

export { Icon } from './icon';

export function MSCheckbox(props) {
  const { label, isWide, style, className, ...rest } = props;

  const cl = {
    'ms-form-check': 1,
    'msfield-wide': isWide,
    [className]: className,
  };

  return (
    <div className={classnames(cl)} style={style}>
      <label className="ms-form-check-label" htmlFor={rest.name}>
        <MSInputCheck {...rest} />
        {label}
      </label>
    </div>
  );
}

export function MSInputCheck(props) {
  let { name, label, error, value, format, ...rest } = props;

  const v = { 0: false, 1: true, false: false, true: true, N: false, Y: true };
  if (format) {
    v[format[0]] = true;
    v[format[1]] = false;
  }
  let checked = v[value];
  checked = (checked === undefined ? false : checked);

  return <input type="checkbox" className="form-check-input" id={rest.id || name} name={name} {...rest} checked={checked} />;
}

export function MSRadio(props) {
  let { label, error, inline, ...rest } = props;

  let cl = {
    'ms-form-radio': 1,
    'form-check-inline': inline,
  };
  return (
    <div className={classnames(cl)}>
      <label className="form-check-label">
        <input className="form-check-input" type="radio" {...rest} checked={rest.id === rest.value} />
        {label}
      </label>
    </div>
  );
}

export function MSRadioGroup(props) {
  const { isWide, children, ...rest } = props;


  const style = { marginBottom: '10px', marginLeft: '20px' };
  if (isWide) style.width = '100%';

  return (
    <div style={style} {...rest} className="form-group">
      {children}
    </div>
  );
}