import React from 'react';
import PropTypes from 'prop-types';
import './icon.css';
import classnames from 'classnames';

export const Icon = (props) => {
  const { icon, name, children, className, ...rest } = props;
  const cl = {
    [className]: !!className,
    'ms-icon': 1,
    'material-icons': 1,
    'ms-icon--button': rest.onClick != null,
  };
  return (
    <span className={classnames(cl)} {...rest}>{icon || name || children}</span>
  );
};

Icon.propTypes = {
  icon: PropTypes.string,
};

Icon.defaultProps = {
  icon: '',
};