import React from 'react';
import classnames from 'classnames';

export { Icon } from './icon';

export function MSAlert(props) {
  let { type, children, ...rest } = props;
  let cn = classnames({
    alert: true,
    ['alert-' + type]: true,
  });
  // if there are something to show
  if (children) {
    return (
      <div className={cn} role="alert" {...rest}>
        {children}
      </div>
    );
  }
  return null;
}